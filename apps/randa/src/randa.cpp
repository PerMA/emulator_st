/*
 * randa.c
 *
 *  Created on: Sep 12, 2014
 *      Author: lloyd23
 */

#include "hpcc.h"
#include "RandomAccess.h"

#include "config.h"
#include "alloc.h"
#include "cache.h"
#include "monitor.h"
#include "ticks.h"
#include "clocks.h"

// TODO: find a better place for these globals

#if defined(STATS) || defined(TRACE) 
XAxiPmon apm;
#endif // STATS || TRACE


int main()
{
	HPCC_Params params;

	MONITOR_INIT

	params.outFname[0] = '\0'; /* use stdout */
	params.HPLMaxProcMem = (size_t)1 << 29; /* half-gig */
	params.RunSingleRandomAccess_LCG = 1;

	/* -------------------------------------------------- */
	/*                 SingleRandomAccess LCG             */
	/* -------------------------------------------------- */

	if (params.RunSingleRandomAccess_LCG) {
		printf("Begin of SingleRandomAccess_LCG section.\n");
		HPCC_RandomAccess_LCG(&params, 1, &params.Single_LCG_GUPs, &params.Failure);
		printf("End of SingleRandomAccess_LCG section.\n");
	}

	TRACE_CAP
	return 0;
}
