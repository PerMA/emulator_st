#!/bin/sh
LC_ALL=C TZ=UTC0 diff -Naur -x "*llfifo*" -x xstreamer.h -x "*.*project" -x "*.o" -x "*.a" standalone_bsp_a9r0 standalone_bsp_a9 >bsp_a9r1_utc.diff
echo "In diff output, delete hunk under system.mss with \"PARAMETER NAME = \"."
