# From open project, type:
# source [get_property DIRECTORY [current_project]]/axi_tcd_src.tcl

set repo_base C:/Xilinx/Vivado/2014.1/data/ip/xilinx
set proj_base [get_property DIRECTORY [current_project]]
# Option: [add_files -norecurse ...]
# To determine the files in a library, look in component.xml
# see xilinx_vhdlbehavioralsimulation_view_fileset
# see xilinx_vhdlsynthesis_view_fileset

set_property target_language VHDL [current_project]

# blk_mem_gen
set blk_mem_gen blk_mem_gen_v8_2
set_property -dict [ list LIBRARY $blk_mem_gen USED_IN {synthesis} ] [add_files $repo_base/$blk_mem_gen/hdl]
set_property -dict [ list LIBRARY $blk_mem_gen USED_IN {simulation} ] [add_files -fileset sim_1 $repo_base/$blk_mem_gen/simulation/blk_mem_gen_v8_2.vhd]

# fifo_generator
set fifo_generator fifo_generator_v12_0
set_property -dict [ list LIBRARY $fifo_generator USED_IN {synthesis simulation} ] [add_files $repo_base/$fifo_generator/hdl]
set_property -dict [ list LIBRARY $fifo_generator USED_IN {simulation} ] [add_files -fileset sim_1 $repo_base/$fifo_generator/simulation/fifo_generator_vhdl_beh.vhd]

# axi_tcd
set axi_tcd axi_tcd_lib
set_property -dict [ list LIBRARY $axi_tcd USED_IN {synthesis simulation} ] [add_files $proj_base/hdl/vhdl/fifo_ic.vhd $proj_base/hdl/vhdl/axi_tcd.vhd]
set_property top axi_tcd [current_fileset]

# test bench
set_property -dict [ list LIBRARY work USED_IN {simulation} ] [add_files -fileset sim_1 $proj_base/hdl/example/axi_blk_mem.vhd $proj_base/hdl/example/axi_tcd_tb.vhd]
set_property top axi_tcd_tb [get_filesets sim_1]
