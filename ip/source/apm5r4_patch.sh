#!/bin/sh
SRC_DIR=/cygdrive/c/Xilinx/Vivado/2014.1/data/ip/xilinx/axi_perf_mon_v5_0
DST_DIR=axi_perf_mon_v5_0
cp -r --preserve=mode,timestamps $SRC_DIR $DST_DIR
cd $DST_DIR
patch -Np1 <../apm5r4_utc.diff
